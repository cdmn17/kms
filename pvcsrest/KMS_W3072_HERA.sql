/* Naam......: KMS_W3072_HERA.sql
** Release...:W3072
** Datum.....: 22-03-2005 09:49:28
** Notes.....: Aangemaakt met ITStools Versie 1.03
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;


WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_KMS en de tablespaces
   KMS_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_KMS';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'KMS_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_KMS bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'KMS', 'W3072', sysdate );

commit;


/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_KMS');
alter user COMP_KMS identified by COMP_KMS;
connect COMP_KMS/COMP_KMS@&&DBN
alter user COMP_KMS identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

@@ KMS.kms_gewichten.tab
@@ KMS.kms_hlp_meetwaardes.tab
@@ KMS.kms_hlp_metingsgevallen.tab
@@ KMS.kms_meeteenheden.tab
@@ KMS.kms_meetpunten.tab
@@ KMS.kms_meetwaardes.tab
@@ KMS.kms_metingsgevallen.tab
@@ KMS.gwt_med_fk_frgn.ind
@@ KMS.gwt_mpt_fk_i.ind
@@ KMS.mgl_med_fk_frgn.ind
@@ KMS.mpt_med_fk_frgn.ind
@@ KMS.mwe_mgl_fk_frgn.ind
@@ KMS.mwe_mpt_fk_frgn.ind
@@ KMS.kms_gwt_pk.con
@@ KMS.kms_hme_pk.con
@@ KMS.kms_hmg_pk.con
@@ KMS.kms_med_pk.con
@@ KMS.kms_mgl_pk.con
@@ KMS.kms_mpt_pk.con
@@ KMS.kms_mwe_pk.con
@@ KMS.kms_gwt_med_fk2.con
@@ KMS.kms_gwt_mpt_fk1.con
@@ KMS.kms_mgl_med_fk1.con
@@ KMS.kms_mpt_med_fk1.con
@@ KMS.kms_mwe_mgl_fk1.con
@@ KMS.kms_mwe_mpt_fk1.con
@@ KMS.kms_gwt_uk1.con
@@ KMS.kms_mpt_uk1.con
@@ KMS.avcon_967101_soort_000.con
@@ KMS.avcon_967104_aspec_000.con
@@ KMS.avcon_967107_beoor_000.con
@@ KMS.kms_v_mpt_per_user.vw
@@ KMS.kms_v_mpt_scores.vw
@@ KMS.kms_gwt_seq.sqs
@@ KMS.kms_med_seq.sqs
@@ KMS.kms_mgl_seq.sqs
@@ KMS.kms_mpt_seq.sqs
@@ KMS.kms_mwe_seq.sqs
@@ KMS.kms_gwt_bri.trg
@@ KMS.kms_gwt_bru.trg
@@ KMS.kms_med_bri.trg
@@ KMS.kms_med_bru.trg
@@ KMS.kms_mgl_bri.trg
@@ KMS.kms_mgl_bru.trg
@@ KMS.kms_mpt_bri.trg
@@ KMS.kms_mpt_bru.trg
@@ KMS.kms_mwe_bri.trg
@@ KMS.kms_mwe_bru.trg

/* creatie database-rollen
*/
connect /@&&DBN
@@ KMS0010F.rol
@@ KMS0020F.rol
@@ KMS0030F.rol
@@ KMS0040R.rol
@@ KMS0050R.rol
@@ KMS0060F.rol

/* KMS_REF_CODES vullen en check constraints plaatsen
*/
connect /@&&DBN
@@ KMS.aspect.dom
@@ KMS.beoordeling.dom
@@ KMS.soort_meeteenheid.dom

connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'KMS', 'W3072 : Klaar', sysdate );

commit;

prompt Implementatie KMS release W3072 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

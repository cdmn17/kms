/* Naam......: KMS_P5146.sql
** Release...:P5146
** Datum.....: 27-03-2007 11:45:05
** Notes.....: Gegenereerd door ITStools Versie 2.15
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;


WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_KMS en de tablespaces
   KMS_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_KMS';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'KMS_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_KMS en/of KMS_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'KMS', 'P5146', sysdate );

commit;


/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_KMS');
alter user COMP_KMS identified by COMP_KMS;
connect COMP_KMS/COMP_KMS@&&DBN
alter user COMP_KMS identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

set define off
set define on
set define off
set define on

/* creatie database-rollen
*/
connect /@&&DBN
@@ KMS0080F.rol

connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'KMS', 'P5146 : Klaar', sysdate );

commit;

prompt Implementatie KMS release P5146 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

/* Naam......: KMS_W9666.sql
** Release...: W9666
** Datum.....: 29-12-2014 13:57:40
** Notes.....: Gegenereerd door ItsTools Versie 4.2.1.0
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_KMS en de tablespaces
   KMS_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_KMS';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'KMS_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_KMS en/of KMS_C bestaat niet in deze database,
*          neem contact op met bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'KMS', 'W9666', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste 11g schema voor KMS.kms_insert_hst_kms0040r_kms0050r.sql
*/
set define on
connect /@&&DBN
@su COMP_KMS

prompt KMS.kms_insert_hst_kms0040r_kms0050r.sql
@@ KMS.kms_insert_hst_kms0040r_kms0050r.sql

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'KMS', 'W9666 : Klaar', sysdate );

commit;

prompt Implementatie KMS release W9666 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on

-- inserten module en parameterwaarden voor de rapportage KMS0040R en KMS0050R

update COMP_HST.QMS_MODULES
set NAME               = 'LEZEN SCORES PER MEETPUNT'
   , title             = 'LEZEN SCORES PER MEETPUNT'
   ,PURPOSE            = 'LEZEN SCORES PER MEETPUNT'
where id = 30;

update COMP_HST.QMS_MODULES
set NAME               = 'LEZEN JUISTHEIDSRAPPORTAGE'
   , title             = 'LEZEN JUISTHEIDSRAPPORTAGE'
   ,PURPOSE            = 'LEZEN JUISTHEIDSRAPPORTAGE'
where id = 31;
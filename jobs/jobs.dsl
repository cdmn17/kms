/* JOB DSL configuration script for application/domain KMS */
job_defaults {
  jenkins_folder = 'Build'
  application = 'KMS'
}

jobs {

  KMS_DATAFIX_PVD {
    type = 'DATAFIX'
    branch = 'database/pvd/datafixes'
    depth=0
  }

  KMS_DB_PVD {
    type = 'DB'
    branch = 'database/pvd'
    sonar_project='sonar-project.properties'
    sonar_properties='sonar.projectBaseDir=src/COMP_KMS'
  }

  KMS_FRM {
    type = 'FRM'
    branch = 'frm'
  }

  KMS_CMP {
    type = 'CMP'
    branch = 'componenten'
    depth = 0
  }

  KMS_RUN_ALL_BLD {
    type = 'BLDALL'
    soort = 'BLD'
  }

  KMS_RELEASE_BRANCH {
    type = 'BRANCH'
    depth = 0
    branch = 'KMS'
    mds = 'N'
  }
}

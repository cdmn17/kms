CREATE OR REPLACE TRIGGER comp_kms.KMS_GWT_BRI
 BEFORE INSERT
 ON comp_kms.KMS_GEWICHTEN
 FOR EACH ROW

BEGIN
:new.creatiemoment:=SYSDATE;
:new.mutatiemoment:=SYSDATE;
:new.creator:=USER;
:new.mutator:=USER;
END KMS_GWT_BRI;
/
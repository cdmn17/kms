CREATE OR REPLACE TRIGGER comp_kms.KMS_MPT_BRI
 BEFORE INSERT
 ON comp_kms.KMS_MEETPUNTEN
 FOR EACH ROW

BEGIN
:new.mutatiemoment:=SYSDATE;
:new.mutator:=USER;
:new.creatiemoment:=SYSDATE;
:new.creator:=USER;
END KMS_MPT_BRI;
/
CREATE OR REPLACE TRIGGER comp_kms.KMS_MED_BRI
 BEFORE INSERT
 ON comp_kms.KMS_MEETEENHEDEN
 FOR EACH ROW

BEGIN
:new.mutatiemoment:=SYSDATE;
:new.mutator:=USER;
:new.creatiemoment:=SYSDATE;
:new.creator:=USER;
END KMS_MED_BRI;
/
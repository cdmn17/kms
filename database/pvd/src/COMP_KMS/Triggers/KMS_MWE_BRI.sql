CREATE OR REPLACE TRIGGER comp_kms.KMS_MWE_BRI
 BEFORE INSERT
 ON comp_kms.KMS_MEETWAARDES
 FOR EACH ROW

BEGIN
:new.mutatiemoment:=SYSDATE;
:new.mutator:=USER;
:new.creatiemoment:=SYSDATE;
:new.creator:=USER;
END KMS_MWE_BRI;
/
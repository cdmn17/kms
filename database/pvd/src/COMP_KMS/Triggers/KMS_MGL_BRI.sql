CREATE OR REPLACE TRIGGER comp_kms.KMS_MGL_BRI
 BEFORE INSERT
 ON comp_kms.KMS_METINGSGEVALLEN
 FOR EACH ROW

BEGIN
:new.mutatiemoment:=SYSDATE;
:new.mutator:=USER;
:new.creatiemoment:=SYSDATE;
:new.creator:=USER;
END KMS_MGL_BRI;
/
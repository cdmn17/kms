CREATE OR REPLACE FORCE VIEW comp_kms.kms_v_mpt_scores (naam_meeteenheid,naam_meetpunt,gewichtsfactor,aantal_nvt,aantal_juist,aantal_onjuist,maximum_score,score,med_id,mpt_id,naam_meeteenheid2,rgg_code,datum_steekproef,datum_mutatie,gebruiker,creatiemoment,gwt_id) AS
SELECT med.naam naam_meeteenheid
, mpt.naam naam_meetpunt
, gwt.gewichtsfactor
, decode(mwe.beoordeling,'N',1,0) aantal_nvt
, decode(mwe.beoordeling,'J',1,0) aantal_juist
, decode(mwe.beoordeling,'O',1,0) aantal_onjuist
, decode(mwe.beoordeling,'N',0,gwt.gewichtsfactor) MAXIMUM_SCORE
, decode(mwe.beoordeling,'J',gwt.gewichtsfactor,0) SCORE
, med.id
, mpt.id
, med2.naam naam_meeteenheid2
, mgl.rgg_code
, mgl.datum_steekproef
, mgl.datum_mutatie
, mgl.gebruiker
, mgl.creatiemoment
, gwt.id
from kms_metingsgevallen mgl
, kms_meetwaardes mwe
, kms_meetpunten mpt
, kms_meeteenheden med
, kms_gewichten gwt
, kms_meeteenheden med2
  WHERE mgl.id=mwe.mgl_id
and mwe.mpt_id = mpt.id
and gwt.med_id=med2.id
and gwt.mpt_id=mpt.id
and med.id=mpt.med_id
and mgl.datum_steekproef is not null
 ;
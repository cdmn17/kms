CREATE OR REPLACE FORCE VIEW comp_kms.kms_v_mpt_per_user (gebruiker,rgg_code,naam_meeteenheid,naam_meetpunt,med_id,mpt_id) AS
SELECT mgl.gebruiker
, mgl.rgg_code
, med.naam naam_meeteenheid
, mpt.naam naam_meetpunt
, med.id med_id
, mpt.id mpt_id
from kms_metingsgevallen mgl
, kms_meetwaardes mwe
, kms_meetpunten mpt
, kms_meeteenheden med
  WHERE mgl.id=mwe.mgl_id
and mwe.mpt_id = mpt.id
and med.id=mpt.med_id
and mgl.datum_steekproef is not null
group by mgl.gebruiker
, mgl.rgg_code
, med.naam
, mpt.naam
, med.id
, mpt.id
 ;
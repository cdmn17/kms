GRANT SELECT ON comp_kms.kms_meeteenheden TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_kms.kms_meetpunten TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_kms.kms_meetwaardes TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_kms.kms_metingsgevallen TO comp_qrt WITH GRANT OPTION;
CREATE TABLE comp_kms.kms_hlp_meetwaardes (
  "ID" NUMBER(8) NOT NULL,
  meetpunt VARCHAR2(30 BYTE) NOT NULL,
  gevonden_waarde VARCHAR2(60 BYTE),
  CONSTRAINT kms_hme_pk PRIMARY KEY ("ID",meetpunt)
);
CREATE TABLE comp_kms.kms_hlp_metingsgevallen (
  "ID" NUMBER(8) NOT NULL,
  med_id NUMBER(8),
  persoonsnummer NUMBER(8),
  werkgevernummer VARCHAR2(6 BYTE),
  omschrijving VARCHAR2(50 BYTE),
  datum_mutatie DATE,
  gebruiker VARCHAR2(240 BYTE),
  rgg_code VARCHAR2(20 BYTE),
  CONSTRAINT kms_hmg_pk PRIMARY KEY ("ID")
);
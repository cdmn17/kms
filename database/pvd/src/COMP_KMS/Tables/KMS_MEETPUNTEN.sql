CREATE TABLE comp_kms.kms_meetpunten (
  "ID" NUMBER(8) NOT NULL,
  naam VARCHAR2(30 BYTE) NOT NULL,
  med_id NUMBER(8) NOT NULL,
  aspect VARCHAR2(1 BYTE) CONSTRAINT avcon_967104_aspec_000 CHECK (ASPECT IN ('V', 'J', 'T')),
  creator VARCHAR2(30 BYTE) NOT NULL,
  creatiemoment DATE NOT NULL,
  mutator VARCHAR2(30 BYTE) NOT NULL,
  mutatiemoment DATE NOT NULL,
  CONSTRAINT kms_mpt_pk PRIMARY KEY ("ID"),
  CONSTRAINT kms_mpt_uk1 UNIQUE (naam,med_id),
  CONSTRAINT kms_mpt_med_fk1 FOREIGN KEY (med_id) REFERENCES comp_kms.kms_meeteenheden ("ID")
);
COMMENT ON COLUMN comp_kms.kms_meetpunten.creator IS 'Auto-generated by Headstart Utilities';
COMMENT ON COLUMN comp_kms.kms_meetpunten.creatiemoment IS 'Auto-generated by Headstart Utilities';
COMMENT ON COLUMN comp_kms.kms_meetpunten.mutator IS 'Auto-generated by Headstart Utilities';
COMMENT ON COLUMN comp_kms.kms_meetpunten.mutatiemoment IS 'Auto-generated by Headstart Utilities';